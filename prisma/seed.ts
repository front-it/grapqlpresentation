import { PrismaClient } from "@prisma/client";
import { faker } from "@faker-js/faker";
import {DefaultArgs, PrismaClientOptions} from "@prisma/client/runtime/library";

export const clearDatabase = async (
    prisma: PrismaClient<PrismaClientOptions, never, DefaultArgs>,
) => {
    const tablenames = await prisma.$queryRaw<
        Array<{ tablename: string }>
    >`SELECT tablename FROM pg_tables WHERE schemaname='public'`;

    const tables = tablenames
        .map(({ tablename }) => tablename)
        .filter((name) => name !== "_prisma_migrations")
        .map((name) => `"public"."${name}"`)
        .join(", ");

    try {
        await prisma
            .$executeRawUnsafe(`TRUNCATE TABLE ${tables} CASCADE;`)
            .then(() => {
                console.log("Successfully cleared database");
            });
    } catch (error) {
        console.log({ error });
    }
};

const prisma = new PrismaClient();

await clearDatabase(prisma);

const productsCount = 5;
const reviewsCount = 2;
for (let i = 0; i < productsCount; i++) {
    const name = faker.commerce.productName();

    const createdProduct = await prisma.product.create({
        data: {
            name: name,
            slug: faker.helpers.slugify(name).toLowerCase(),
            description: faker.commerce.productDescription(),
            price: Number(faker.commerce.price()) * 100,
        },
    });
    for (let j = 0; j < reviewsCount; j++) {
        const createdReview = await prisma.review.create({
            data: {
                title: faker.lorem.sentence(),
                description: faker.lorem.paragraph(),
                author: `${faker.person.firstName()} ${faker.person.lastName()}`,
                rating: faker.number.int({ min: 1, max: 5 }),
                Product: {
                    connect: {
                        id: createdProduct.id,
                    },
                },
            },
        });
        console.log(`Created review with id: ${createdReview.id}`);
    }
    console.log(`Created product with id: ${createdProduct.id}`);
}
