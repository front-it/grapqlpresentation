import type { MutationResolvers } from "../../../types.generated";
export const createReview: NonNullable<
	MutationResolvers["createReview"]
> = async (_parent, arg, ctx) => {
	return await ctx.prisma.review.create({
		data: {
			rating: arg.data.rating,
			author: arg.data.author,
			Product: {
				connect: {
					id: arg.data?.Product?.connect?.id ?? undefined,
				},
			},
			title: arg.data.title,
			description: arg.data.description,
			createdAt: new Date(),
			updatedAt: new Date(),
		},
	});
};
