import type { QueryResolvers } from "../../../types.generated";
export const review: NonNullable<QueryResolvers["review"]> = async (
	_parent,
	arg,
	ctx,
) => {
	const review = await ctx.prisma.review.findUnique({
		where: { id: arg.id },
	});
	if (!review) {
		return null;
	}
	return review;
};
