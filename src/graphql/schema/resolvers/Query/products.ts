import type { QueryResolvers } from "../../../types.generated";
export const products: NonNullable<
	QueryResolvers["products"]
> = async (_parent, arg, ctx) => {
	return await ctx.prisma.product.findMany({
		take: arg.first ?? undefined,
		skip: arg.skip ?? undefined,
	});
};
