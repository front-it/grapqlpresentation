import type { QueryResolvers } from "../../../types.generated";
export const product: NonNullable<QueryResolvers["product"]> = async (
	_parent,
	arg,
	ctx,
) => {
	const product = await ctx.prisma.product.findUnique({
		where: {
			id: arg.where.id ?? undefined,
		},
	});
	return product ?? null;
};
