import type { QueryResolvers } from "../../../types.generated";
export const reviews: NonNullable<QueryResolvers["reviews"]> = async (
	_parent,
	args,
	ctx,
) => {
	args.first = args.first ?? 20;
	args.skip = args.skip ?? 0;

	return await ctx.prisma.review.findMany({
		take: args.first,
		skip: args.skip,
	});
};
