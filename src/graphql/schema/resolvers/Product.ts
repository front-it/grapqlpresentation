import type { ProductResolvers } from "../../types.generated";
export const Product: ProductResolvers = {
	async productReviews(parent, _args, ctx) {
		// return ctx.prisma.review.findMany({
		// 	where: { productId: parent.id || undefined },
		// });
		return await ctx.prisma.product
			.findUnique({ where: { id: parent.id } })
			.Reviews();
	},

};
